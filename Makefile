obj-m = ksudoku.o
obj-m += simple.o

kernel_version = $(shell uname -r)
build_dir = /lib/modules/$(kernel_version)/build

all:
	$(MAKE) -C $(build_dir) M=$(PWD) modules
.PHONY: load
load:
	insmod ksudoku.ko
	insmod simple.ko
.PHONY: unload
unload:
	rmmod simple.ko
	rmmod ksudoku.ko

.PHONY: send
send:
	@echo "670008010020060000000030000201000006480001700000000009004500000000000300003400802" > /sys/kernel/ksudoku/simple/matrix
clean:
	$(MAKE) -C $(build_dir) M=$(PWD) clean
	rm *~
