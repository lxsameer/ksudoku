#! /bin/bash

IMG=`pwd`/root.img
MOUNT_POINT=`pwd`/mnt
DIR=./root_fs


qemu-img create $IMG 1G
sudo mkfs.ext2 $IMG
mkdir -p $DIR $MOUNT_POINT
sudo mount -o loop $IMG $MOUNT_POINT
sudo debootstrap stable $DIR http://ftp.de.debian.org/debian
sudo mv $DIR/* $MOUNT_POINT/
sudo umount $MOUNT_POINT
sudo rm -rf $MOUNT_POINT
