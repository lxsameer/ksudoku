#! /bin/bash

qemu-system-x86_64 \
    -kernel /lib/modules/`uname -r`/build/arch/x86/boot/bzImage \
    -append "root=/dev/ram init=/init console=ttyS0" \
    -initrd `pwd`/initrd.img \
    -nographic -smp 1 -cpu host --enable-kvm\
    -m 2048 \
    -drive file=fat:rw:`pwd`
