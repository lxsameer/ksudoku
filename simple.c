/**
 * A simple algorithm to solve sudoku for ksudoku subsystem
 * Copyright (C) 2020 Sameer Rahmani <lxsameer@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include "ksudoku.h"

#define SIMPLE_SUDOKU_VERSION "0.1"
#define SIMPLE_SUDOKU_DESC "A simple sudoku algorithm"


static struct ksudoku *sudoku;


static int simple_sudoku_init(void)
{

        printk(KERN_ALERT "Simple sudoku init.\n");

        sudoku = ksudoku_create_sudoku("simple");

        if (!sudoku)
                return -ENOMEM;

        return 0;
}


static void simple_sudoku_exit(void)
{
        ksudoku_destroy_ksudoku(sudoku);
        printk(KERN_ALERT "Exit simple sudoku.\n");
}


MODULE_AUTHOR("Sameer Rahmani <lxsameer@gnu.org>");

MODULE_DESCRIPTION(SIMPLE_SUDOKU_DESC);
MODULE_VERSION(SIMPLE_SUDOKU_VERSION);
MODULE_LICENSE("GPL");

module_init(simple_sudoku_init);
module_exit(simple_sudoku_exit);
