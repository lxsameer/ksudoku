/**
 * KSudoku subsystem for Linux kernel to solve sudoku with different
 * algorithms.
 * Copyright (C) 2020 Sameer Rahmani <lxsameer@gnu.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#ifndef _KSUDOKU_H_
#define _KSUDOKU_H_

#include <linux/init.h>
#include <linux/module.h>
#include <linux/sysfs.h>
#include <asm/atomic.h>
#include <linux/slab.h>

#define KSUDOKU_READY 0
#define KSUDOKU_BUSY 1

struct ksudoku {
        struct kobject kobj;
        const struct attribute_group *attr_group;
        char *matrix;
        int matrix_array[9][9];
        atomic_t status;
};
#define to_ksudoku(obj) container_of(obj, struct ksudoku, kobj)


struct ksudoku_attribute {
	struct attribute attr;
	ssize_t (*show)(struct ksudoku *s, struct ksudoku_attribute *attr,
                        char *buf);
	ssize_t (*store)(struct ksudoku *s, struct ksudoku_attribute *attr,
			 const char *buf, size_t count);
};

#define to_ksudoku_attr(_attr) container_of(_attr, struct ksudoku_attribute, attr)

extern struct ksudoku *ksudoku_create_sudoku(const char *name);
extern void ksudoku_destroy_ksudoku(struct ksudoku *sudoku);
extern bool ksudoku_is_valid_cell(struct ksudoku *sudoku, int v, int row, int col);

#endif
